# Description
When bundling an `elm` application with `parcel` and you have a `browserslist` defined 
either in your `package.json` or `.browserslistrc` file, then you receive the following error:

```
🚨 Build failed.

@parcel/transformer-elm: Unexpected token + in JSON at position 0

  SyntaxError: Unexpected token + in JSON at position 0
      at JSON.parse (<anonymous>)
      at Object.transform (/home/david/Code/elm-parcel-browserslist-bug/node_modules/@parcel/transformer-elm/lib/ElmTransformer.js:145:38)
      at processTicksAndRejections (node:internal/process/task_queues:96:5)
      at async Transformation.runTransformer (/home/david/Code/elm-parcel-browserslist-bug/node_modules/@parcel/core/lib/Transformation.js:653:5)
      at async Transformation.runPipeline (/home/david/Code/elm-parcel-browserslist-bug/node_modules/@parcel/core/lib/Transformation.js:402:36)
      at async Transformation.runPipelines (/home/david/Code/elm-parcel-browserslist-bug/node_modules/@parcel/core/lib/Transformation.js:265:40)
      at async Transformation.run (/home/david/Code/elm-parcel-browserslist-bug/node_modules/@parcel/core/lib/Transformation.js:183:21)
      at async Child.handleRequest (/home/david/Code/elm-parcel-browserslist-bug/node_modules/@parcel/workers/lib/child.js:216:9)
```

# Versions Tested With
```
$ node --version
v16.16.0

$ npm --version
8.19.1

$ uname -a
Linux desktop 5.19.6-arch1-1 #1 SMP PREEMPT_DYNAMIC Wed, 31 Aug 2022 22:09:40 +0000 x86_64 GNU/Linux
```

# Reproduction Steps
1. `git clone https://gitlab.com/Disco-Dave/elm-parcel-browserslist-bug.git`
2. `cd elm-parcel-browserslist-bug`
3. `npm clean-install`
4. `npm run build` <-- should fail
5. Try again, `npm run build` <-- second time works for me

Or with docker:
1. `docker build .`
