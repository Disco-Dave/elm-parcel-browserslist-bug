module Main exposing (main)

import Browser
import Html exposing (text)


main : Program () () ()
main =
    Browser.application
        { init = \_ _ _ -> ( (), Cmd.none )
        , view =
            \_ ->
                { title = "Parcel Bug Demo"
                , body = [ text "it worked, hello!" ]
                }
        , update = \_ _ -> ( (), Cmd.none )
        , subscriptions = \_ -> Sub.none
        , onUrlChange = \_ -> ()
        , onUrlRequest = \_ -> ()
        }
