FROM node:16 AS build

WORKDIR /build

COPY package.json package-lock.json /build/
RUN npm clean-install

COPY elm.json /build/
COPY src/ /build/src/
RUN npm run build
